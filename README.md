# Getting Started with Create React App
## Available Scripts

In the project directory, you can run:

### `npm install | npm update`

Run Json-Server:

### `json-server --watch db.json`

Run Project

### `npm run dev`