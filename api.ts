import { headers } from "next/dist/client/components/headers";
import { ITask } from "./types/tasks";

const baseUrl  = 'http://localhost:3001';

export const getAllTasks = async (): Promise<ITask[]> => {
    const res = await fetch(`${baseUrl}/tasks`, { cache: "no-store" });
    const tasks = await res.json();
    return tasks;
}

export const createTask =async (task: ITask): Promise<ITask> => {
    const res = await fetch(`${baseUrl}/tasks`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(task)
    })
    const newTask = await res.json();
    return newTask;
}

export const editTask =async (task: ITask): Promise<ITask> => {
    const res = await fetch(`${baseUrl}/tasks/${task.id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(task)
    })
    const updateTask = await res.json();
    return updateTask;
}

export const deleteTask =async (id: string): Promise<void> => {
    await fetch(`${baseUrl}/tasks/${id}`, {
        method: 'DELETE',
    })
}

