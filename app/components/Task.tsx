"use client"
import { ITask } from '@/types/tasks'
import React, { FormEventHandler, useState } from 'react'
import { FiEdit, FiTrash } from 'react-icons/fi'
import { BiShow } from 'react-icons/bi'

import { Modal } from './Modal'
import { useRouter } from 'next/navigation'
import { deleteTask, editTask } from '@/api'

interface TaskProps {
    task: ITask
}

export const Task: React.FC<TaskProps> = ({ task }) => {
    const router = useRouter();
    const [modalOpenShow, setModalOpenShow] = useState()

    const [modalOpenEdit, setModalOpenEdit] = useState<boolean>(false);
    const [taskToEdit, setTaskToEdit] = useState<string>(task.text);

    const [modalOpenDelete, setModalOpenDelete] = useState<boolean>(false);

    const handleSubmitEditTask: FormEventHandler<HTMLFormElement> =
        async (e) => {
            e.preventDefault();
            await editTask({
                id: task.id,
                text: taskToEdit
            })
            setModalOpenEdit(false)
            router.refresh();
        }

    const handleDeleteTask = async (id: string) => {
        await deleteTask(id);
        setModalOpenDelete(false);
        router.refresh();
    }

    return (
        <tr key={task.id}>
            <td className='w-full'>{task.text}</td>
            <td className='flex gap-5'>

                <Modal modalOpen={modalOpenEdit} setModalOpen={setModalOpenEdit}>
                    <form onSubmit={handleSubmitEditTask}>
                        <h3 className='font-bold text-lg'>Edit Task</h3>
                        <div className='modal-action'>
                            <input
                                value={taskToEdit}
                                onChange={e => setTaskToEdit(e.target.value)}
                                type="text"
                                placeholder="Type here"
                                className="input input-bordered w-full w-full"
                            />
                            <button type='submit' className='btn'>Submit</button>
                        </div>
                    </form>
                </Modal>

                <FiEdit onClick={() => setModalOpenEdit(true)} cursor='pointer' className='text-blue-500' size={25} />
                <Modal modalOpen={modalOpenEdit} setModalOpen={setModalOpenEdit}>
                    <form onSubmit={handleSubmitEditTask}>
                        <h3 className='font-bold text-lg'>Edit Task</h3>
                        <div className='modal-action'>
                            <input
                                value={taskToEdit}
                                onChange={e => setTaskToEdit(e.target.value)}
                                type="text"
                                placeholder="Type here"
                                className="input input-bordered w-full w-full"
                            />
                            <button type='submit' className='btn'>Submit</button>
                        </div>
                    </form>
                </Modal>

                <FiTrash onClick={() => setModalOpenDelete(true)} cursor='pointer' className='text-red-500' size={25} />
                <Modal modalOpen={modalOpenDelete} setModalOpen={setModalOpenDelete}>
                    <h3 className='text-lg'>Are you sure, you want to delete this task ?</h3>
                    <div className='modal-action'>
                        <button
                            className='btn'
                            onClick={() => handleDeleteTask(task.id)}
                        >
                            Yes
                        </button>
                    </div>
                </Modal>
            </td>
        </tr>
    )
}
