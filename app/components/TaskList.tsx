import { ITask } from '@/types/tasks'
import React from 'react'
import { Task } from './Task'

interface TaskListProps {
    tasks: ITask[]
}

const TaskList: React.FC<TaskListProps> = ({ tasks }) => {
  return (
    <div className="overflow-x-auto mt-5">
        <table className="table w-full">
            {/* head*/}
            <thead>
            <tr>
                <th>Task Name</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            {tasks.map((task) => <Task key={task.id} task={task} />)}
           
            </tbody>
        </table>
</div>
  )
}

export default TaskList;
