import React from 'react'
import CreateTask from './components/CreateTask'
import TaskList from './components/TaskList'
import { getAllTasks } from '@/api'

export default async function Home() {
  const tasks = await getAllTasks();
  console.log(tasks)

     return(
      <main className='max-w-4xl mx-auto mt-4'>
        <div className='text-center my-5 flex flex-col gap 4'>
          <h1 className='text-2xl font-bold'>List Task App</h1>
        </div>
        <CreateTask />
        <TaskList tasks = {tasks} />
      </main>
     )     
}
